from flask import Flask

app = Flask(__name__)


@app.route('/')
def hello():
  return '<body style="background-color: #F5B041">Hello, this simple Flask!</body>'


@app.route('/other')
def other_page():
  return 'Other flask page oh!'


if __name__ == '__main__':
  app.run()
